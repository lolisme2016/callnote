package com.example.waracci.callnote;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.name;
import static android.os.Build.VERSION_CODES.O;

public class SelectedContactsActivity extends AppCompatActivity {
    private SharedPreferences mSharedPreferences;
    private String mRecentContacts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_contacts);



        Button mBtn = (Button) findViewById(R.id.secondLoadContacts);

        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ContactsActivity.class);
                startActivity(intent);
            }
        });

//        Bundle bundle = getIntent().getExtras();
//        final String[] selectedContactsArray = bundle.getStringArray("selectedContacts");
        ListView mSelectedContactsListView = (ListView) findViewById(R.id.selectedContactsListView);



         final String[] contactList;

        //retrieve data from shared prefs
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mRecentContacts = mSharedPreferences.getString("contacts", null);
        Log.v("lolan1contacts", mRecentContacts);
        contactList = mRecentContacts.split(",");
        Log.v("lolan2contactlist", contactList.toString());

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.single_item_listview, contactList);
        mSelectedContactsListView.setAdapter(arrayAdapter);
        mSelectedContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), contactList[i], Toast.LENGTH_SHORT).show();
                String contactName = ((TextView) view).getText().toString();

                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);

                intent.putExtra("contact", contactName);
                startActivity(intent);
            }
        });

    }
}
