package com.example.waracci.callnote;

/**
 * Created by waracci on 10/6/17.
 */
public class Contact {
    private String name;
    private String phone;
    private String reminder;
    private long date;
    private String pushId;

    public Contact(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName(){
        return name;
    }
    public String getPhone () {
        return phone;
    }
    public String getReminder() {
        return reminder;
    }
    public long getDate() {
        return date;
    }
    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }
}
