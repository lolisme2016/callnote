package com.example.waracci.callnote;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

    private TextView mTextMessage;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    getContactDetails();
                case R.id.navigation_dashboard:
                    setReminder();
                    return true;
                case R.id.navigation_notifications:
                    setTimer();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mTextMessage = (TextView) findViewById(R.id.message);

       getContactDetails();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void getContactDetails() {
        Intent intent = getIntent();
        //retrieve data attached to intent
        String contactName = intent.getStringExtra("contact");
        mTextMessage.setText(contactName + " details");
    }

    public void setReminder() {
        Toast.makeText(getApplicationContext(), "You clicked on set reminder", Toast.LENGTH_SHORT).show();
    }

    public void setTimer() {
        Toast.makeText(getApplicationContext(), "You clicked on set timer", Toast.LENGTH_SHORT).show();
    }
}
