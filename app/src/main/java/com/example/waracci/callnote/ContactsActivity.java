package com.example.waracci.callnote;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;



public class ContactsActivity extends AppCompatActivity {
    Cursor cursor;
    int counter;
    private ProgressDialog progressDialog;
    private Handler updateBarHandler;

    //include my list view
    private ListView mContactsListView;
    private ArrayAdapter<String> arrayAdapter;
    ArrayList<Contact> contactHolderArrayList;
    ArrayList<String> contactNamesList = new ArrayList<String>();
    ArrayList<String> contactPhoneNumberList = new ArrayList<String>();
    private Button loadContactsButton;
    private Button mViewSelectedButton;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        //progress bar handler
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Retrieving contacts...");
        progressDialog.setCancelable(false);

        updateBarHandler = new Handler();



        loadContactsButton = (Button) findViewById(R.id.loadContactsButton);
        loadContactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        showConsentDialog();
                        retrieveContacts();
                        Looper.prepare();
                    }
                }).start();
            }
        });

        mContactsListView = (ListView)findViewById(R.id.ContactsListView);
        mContactsListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        mContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.v("lolisme", arrayAdapter.getItem(position).toString());
                //lol
            }
        });

        mViewSelectedButton = (Button) findViewById(R.id.viewSelectedButton);
        mViewSelectedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SparseBooleanArray checked = mContactsListView.getCheckedItemPositions();
//                ContactsArrayAdapter contactsArrayAdapter = new ContactsArrayAdapter(getApplicationContext(), res, contactNamesList, contactPhoneNumberList);

                ArrayList<String> checkedContacts = new ArrayList<String>();

                mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                mEditor = mSharedPreferences.edit();
                StringBuilder stringBuilder = new StringBuilder();

                for (int i = 0; i < checked.size(); i++) {
                    //get item positions in adapter
                    int checkedPosition = checked.keyAt(i);
                    //add spot if clicked
                    if (checked.valueAt(i))
                        checkedContacts.add(arrayAdapter.getItem(checkedPosition));
                        /**
                         * lotsa trouble here
                         * **/
//                        checkedContacts.add((contactsArrayAdapter.getItem(checkedPosition)).toString());

                        Log.v("lolisme", checkedContacts.get(i));
                        System.out.println(checkedContacts.size());

                }


                    String[] outputStr = new String[checkedContacts.size()];

                    for (int j = 0; j < checkedContacts.size(); j++){
                        outputStr[j] = checkedContacts.get(j);
                        stringBuilder.append(checkedContacts.get(j)).append(",");

                    }

                //send to shared prefs

                    //Log.v("lolan", stringBuilder.toString());
                    mEditor.putString("contacts", stringBuilder.toString());

                    mEditor.commit();

                //alright
                Intent intent = new Intent(getApplicationContext(), SelectedContactsActivity.class);

                //create a bundle object

                Bundle bundle = new Bundle();
                bundle.putStringArray("selectedContacts", outputStr);
                //add the bundle to the intent
                intent.putExtras(bundle);
                startActivity(intent);



                //intent here mate

            }
        });
    }

    //target minimum apk sdk
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void retrieveContacts() {
        String phoneNumber = null;
        String contactName;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        //Contact contact; //= new Contact(DISPLAY_NAME, NUMBER);

        ContentResolver contentResolver = getContentResolver();
         cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
        //we iterate through every contact on the phone.

        if (cursor.getCount() > 0) {
            counter = 0;
            while (cursor.moveToNext()) {
                //model declaration ?

                //update progress message
                updateBarHandler.post(new Runnable(){
                    @Override
                    public void run() {
                        progressDialog.setMessage("Reading Contacts " + counter++ + "/" + cursor.getCount());
                    }
                });
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                contactName = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                   // Log.v("lolisme", contactName);
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{ contact_id }, null);
                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));

                    }
                    phoneCursor.close();
                }
                Contact contact = new Contact(contactName, phoneNumber);
                String obtname = contact.getName();
                String obtPhone = contact.getPhone();
                contactNamesList.add(obtname.toString());
                contactPhoneNumberList.add(obtPhone.toString());
                //Log.v("lolisme", obtname + obtPhone);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //update my listview and ui here
                    //check whether data is packaged correctly
//                    for (String data : contactNamesList) {
//                        Log.v("dataInArray", data);
//                    }

                    /****
                     * deprecated upon usage of the new array adapter**/

                    arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_list_item_multiple_choice, contactNamesList);

                    mContactsListView.setAdapter(arrayAdapter);

                    /****
                     * deprecated upon usage of the new array adapter**/
//                    ContactsArrayAdapter contactsArrayAdapter = new ContactsArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_multiple_choice, contactNamesList, contactPhoneNumberList);
//                    mContactsListView.setAdapter(contactsArrayAdapter);
                }
            });

            //dismiss the progress bar and display a dialog

            updateBarHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.cancel();
                    // display dialog here
                    showSuccessDialog();
                }
            }, 500);
        }
    }

    public void showSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("Success");
        builder.setMessage("Reading contacts completed");

        String positiveText = "ok";
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //positive button logic
            }
        });
        AlertDialog dialog = builder.create();
        //display dialog
        dialog.show();
    }
    public void showConsentDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("Confirm");
        builder.setMessage("This action is about to read your personal contacts.");

        String positiveText = "ok";
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //positive button logic

            }
        });
        //Looper.prepare();
        AlertDialog dialog = builder.create();
        //display dialog
        dialog.show();
    }

}
