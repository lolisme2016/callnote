package com.example.waracci.callnote;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by waracci on 10/7/17.
 */

public class ContactsArrayAdapter extends ArrayAdapter{
    private Context mContext;
    private ArrayList<String> mNames;
    private ArrayList<String> mPhones;

    public ContactsArrayAdapter(Context mContext, int resource, ArrayList<String> mNames, ArrayList<String> mPhones) {
        super(mContext, resource);
        this.mContext = mContext;
        this.mNames = mNames;
        this.mPhones = mPhones;
    }

    @Override
    public Object getItem(int position) {
        String name = mNames.get(position);
        String phone = mPhones.get(position);
        return String.format("%s %s", name, phone);
    }

    @Override
    public int getCount() {
        return mNames.size();
    }
}
